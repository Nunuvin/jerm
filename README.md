# A terminal app written in java.
Avaliable under GPL v3

## 0.0.5 able to run programs with absolute path provided. Can use arguments.
Does not support direct interaction between user and a subprocess (so all programs
relying on user input probably wont work).
Not Ansi escape code compatible.