package jerm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * deals with processes run by shell
 * @author nunuvin
 *
 */
public class Executor {
	String stdin;
	String stdout;
	String stderr;
	
	public String getStdin() {
		return stdin;
	}
	
	public void setStdin(String stdin) {
		this.stdin = stdin;
	}
	
	public void pushToStdin(String s) {
		
	}
	
	public String getStdout() {
		return stdout;
	}

	public String getStderr() {
		return stderr;
	}
	
	public void execute(String exec) {
		String[] spl = exec.split(" ");

		
		Process process;
		try {
			process = new ProcessBuilder(spl).start();
			InputStream is = process.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line;
			
			TermRenderer Tr = new TermRenderer();
			Tr = Tr.getTermRenderer();

			while ((line = br.readLine()) != null) {
			  //System.out.println(line);
			  Tr.print(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
