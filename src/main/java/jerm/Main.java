package jerm;

import javafx.application.Application;

import javafx.scene.*;
import javafx.stage.Stage;

import javafx.scene.paint.*;
import javafx.scene.canvas.*;
import javafx.scene.input.KeyEvent;


import jerm.TermRenderer;
import jerm.CommandState;

@SuppressWarnings("restriction")
public class Main  extends Application{

	/**
	 * starts the program
	 * @param args passed when started. Expected none.
	 */
	public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) {
    	//set up wiondow
        primaryStage.setTitle("JERM 0.0.5");
        TermRenderer Tr = new TermRenderer();
        Tr = Tr.getTermRenderer();
        
        Group root = new Group();
        Scene MainScene = new Scene(root, Tr.getWidth(), Tr.getHeight(), Color.BLACK);
        primaryStage.setScene(MainScene);
        
        //canvas on which everything is rendered
        final Canvas canvas = new Canvas(Tr.getWidth(),Tr.getHeight());
        Tr.setGc(canvas.getGraphicsContext2D());
         
        CommandState ComState = new CommandState();
        		
        MainScene.addEventFilter(KeyEvent.KEY_TYPED, (event) -> ComState.handleTyped(event));
        MainScene.addEventFilter(KeyEvent.KEY_PRESSED, (event) -> ComState.handlePressed(event));
         
        root.getChildren().add(canvas);
        
        
        primaryStage.show();
    }
}
