package jerm;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import java.lang.Math;
import java.util.ArrayList;


@SuppressWarnings("restriction")
/**
 * deals with rendering info to the terminal
 * SINGLETON
 * @author nunuvin
 *
 */
public class TermRenderer {
	
	static TermRenderer tr;
	
	
	private Font font;
	private int width = 800; //canvas dimensions
	private int height = 600;
	
	private int maxChars; //in a line
	private int maxLines; //max number of lines in a screen
	
	private int horizontalOffset = 10; 
	private int verticalOffset = 20;
	private double fullnessModifier = 0.98; // manages offset on bot and right
	
	private int CurLineLen = 0; //keeps track of length of current line being printed
	private int CurLineNum = 0; //keeps track which line being printed
	
	private GraphicsContext gc; //grafic context from canvas
	private Color FGColor = Color.WHITE; //foreground color
	private Color BGColor = Color.BLACK; //background color
	
	public TermRenderer() {}
	
	public TermRenderer getTermRenderer() { //singleton implementation
		if(tr == null) {
			tr = new TermRenderer();
		}
		return tr;
			
	}
	
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	
	/**
	 * set gc,
	 * also determine the default font and derive maxChars and maxLines
	 * @param gc
	 */
	public void setGc(GraphicsContext gc) {
		this.gc = gc;
		font = gc.getFont();
		
		maxChars = (int) Math.round(width / font.getSize() * fullnessModifier)-1;
		maxLines = (int) Math.round(height / font.getSize() * fullnessModifier)-1;
	}
	
	public GraphicsContext getGc() {
		return gc;
	}
	
	/**
	 * clears the screen by filling
	 */
	public void clearScreen() {
		CurLineLen = 0;
		CurLineNum = 0;
		gc.setFill(BGColor);
		gc.fillRect(0, 0, width, height);
	}
	
	/**
	 * moves on to new line except when screen is full then cls
	 */
	public void newLine() {
		CurLineLen = 0;
		CurLineNum++;
		if(CurLineNum > maxLines) { //bottom is too close
			clearScreen();
		}
	}
	
	/**
	 * prints the String of size 1
	 * @param s a SINGLE character to be printed
	 */
	public void printChar(String s) {

			if(CurLineLen > maxChars) {
				newLine();
			}
			gc.setFill(FGColor);
			gc.fillText(s,
						horizontalOffset + CurLineLen * font.getSize(),
						verticalOffset + CurLineNum * font.getSize());
			CurLineLen++;

	}
	
	public void print(String s) {
		//System.out.println(s);
		for(int i=0;i<s.length();i++) {
			printChar(Character.toString(s.charAt(i)));
		}
	}
	
	public int getMaxLines() {
		return maxLines;
	}

	public int getCurLineNum() {
		return CurLineNum;
	}

	/**
	 * erase a single char
	 */
	public void eraseChar() {
		if(CurLineLen != -1) {
			gc.setFill(BGColor);
			gc.fillRect(horizontalOffset + (CurLineLen - 1) * font.getSize(),
						verticalOffset + (CurLineNum - 1) * font.getSize(),
						font.getSize(),
						font.getSize() * 1.25); //deal with y
			CurLineLen--;
		}
	}
	
	public void renderScreenful(int top, ArrayList<String> cmemory) {
		clearScreen();
		for(int i=top;i<top+maxLines && i<cmemory.size();i++) {
			
			String s = cmemory.get(i);
				for(int j=0; j<s.length();j++) {
					printChar( Character.toString(s.charAt(j)));
				}
				newLine();
			}
			
		}
		
	}

