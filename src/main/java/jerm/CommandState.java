package jerm;

import javafx.scene.input.KeyEvent;
import java.lang.StringBuilder;
import java.util.ArrayList;


import jerm.TermRenderer;

@SuppressWarnings("restriction")
/**
 * deals with reading from keyboard and storing or passing commands
 * @author nunuvin
 *
 */
public class CommandState {
	private StringBuilder CurCommand;
	private String CommandPassed;
	private ArrayList<String>CommandMemory;
	private int curMemLocation;
	
	public CommandState() {
		CurCommand = new StringBuilder();
        CommandPassed = new String();
        CommandMemory = new ArrayList<String>(100);
        curMemLocation = 0;
	}

	public StringBuilder getCurCommand() {
		return CurCommand;
	}


	public String getCommandPassed() {
		return CommandPassed;
	}

	/**
	 * handle keypresses in typed event
	 * @param event
	 */
    public void handleTyped(KeyEvent event) {
    	TermRenderer Tr = new TermRenderer();
    	Tr = Tr.getTermRenderer();
    	
    	switch(event.getCharacter()) {
    	case "\r": //enter
    	case "\n":
    		
    		CommandPassed = CurCommand.toString();
    		//System.out.println(CommandPassed);
    		CurCommand.delete(0, CurCommand.length());
    		Tr.newLine();
    		rememberCommand(CommandPassed);
    		Executor Ex = new Executor();
    		Ex.execute(CommandPassed);
    		
    		break;
    	case "\b": //backspace
    		
    		if(CurCommand.length() != 0) {
	    		CurCommand.delete(CurCommand.length() - 1, CurCommand.length());
	    		Tr.eraseChar();
	    		//System.out.println(CurCommand.toString());
    		}
    		
    		break;
    		
    	default:
    		//System.out.println(event.getCharacter());
    		CurCommand.append(event.getCharacter());
    		Tr.printChar(event.getCharacter());
    	}
    	
    }
    
    /**
     * deals with non typeable combinations ie page up/page down
     * @param event
     */
    public void handlePressed(KeyEvent event) {
    	TermRenderer Tr = new TermRenderer();
    	Tr = Tr.getTermRenderer();
    	
    	int newTopLine;
    	switch(event.getCode()) {
    	case PAGE_UP:
    		newTopLine = curMemLocation - Tr.getMaxLines();
    		if(newTopLine >= 0) {
    			Tr.renderScreenful(newTopLine, CommandMemory);
    			curMemLocation = newTopLine;
    		}else {
    			Tr.renderScreenful(0, CommandMemory);
    			curMemLocation = 0;
    		}
    		
    		break;
    		
    	case PAGE_DOWN:
    		newTopLine = curMemLocation + Tr.getMaxLines();
    		if(newTopLine  <= CurCommand.length()-Tr.getMaxLines() && newTopLine >= 0) {
    			Tr.renderScreenful(newTopLine, CommandMemory);
    			curMemLocation = newTopLine;
    		}else if(CommandMemory.size() - Tr.getMaxLines()>0){
    			Tr.renderScreenful(CommandMemory.size()-Tr.getMaxLines(), CommandMemory);
    		}
    		break;
    	default:
    			
    	}
    }
    
    /**
     * 
     * @param s add to commandMemory
     */
    private void rememberCommand(String s) {
    	if (CommandMemory.size() == 1000) {
    		CommandMemory.remove(0);
    		curMemLocation--;
    	}
    	CommandMemory.add(s);
    	curMemLocation++;
    }
}
